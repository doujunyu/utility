<?php
namespace doujunyu\utility\allinpay;

//$demo->depositApply_QUICKPAY_VSP("1203258183",1,0,"6227000267060250071");						//充值申请（收银宝快捷入金）
//$demo->depositApply_GATEWAY_VSP("1203258183",5,0);											//充值申请（收银宝网关入金）
//$demo->payFront("1203258183","TL20200424123632"); 											//确认支付（前台+短信验证码确认）
//$demo->payBGCode();																			//确认支付（后台+短信验证码确认）
//$demo->withdrawApply("1203258183",1,0,"6227000267060250071");									//提现申请（通联通代付出金）
//$demo->withdrawApply_HT();																	//提现申请（华通代付出金）
//$demo->consumeApply_BALANCE("1203258183",1,0);												//消费申请（余额入金)
//$demo->consumeApply_GATEWAY_VSP("1203258183","TLSXTESTUSER",1,0);								//消费申请（收银宝网关入金）
//$demo->consumeApply_QUICKPAY_TLT("1203258183","TLSXTESTUSER",1,0,"6227000267060250071");		//消费申请（通联通协议支付入金)
//$demo->consumeApply_WECHAT_PUBLIC("1203258183","TLSXTESTUSER",1,0);							//消费申请 (微信JS支付)
//$demo->consumeApply_ALIPAY_APP_OPEN("1203258183","TLSXTESTUSER",1,0);							//消费申请 (支付宝APP)
//$demo->agentCollectApply("1203258183");													    //托管代收申 (TLSXTEST0009，TLSXTEST0010）
//$demo->signalAgentPay09("TLSXTEST0009","TL20200424010132");									//单笔托管代付（向TLSXTEST0009用户付款）
//$demo->signalAgentPay10("TLSXTEST0010","TL20200424010132");									//单笔托管代付（向TLSXTEST0010用户付款）
//$demo->refund("1203258183","TL20200424010132");												//退款申请（1203258183退款，已全部代付成功）
//$demo->applicationTransfer("1203258183",20);													//平台转账申请
//$demo->queryBalance("1203258183");															//查询余额
//$demo->getOrderDetail("TL20200424011231");													//查询订单状态
//$demo->queryInExpDetail("TLSXTESTUSER");														//查询账户收支明细
//$demo->getPaymentInformationDetail("");														//付款方资金代付明细查询
//$demo->getPayeeFundsInTransitDetail("","");													//收款方在途资金明细查询
//$demo->queryReserveFundBalance();																//通联通头寸查询
//$demo->getCheckAccountFile();																	//平台集合对账下载
//$demo->queryMerchantBalance();																//平台账户集余额查询
//$demo->createBankSubAcctNo("1203258183");														//个人开户

use doujunyu\utility\allinpay\sdk\YunClient;

class AllinPayOrder
{
    private  $logIns;
    private  $config;
    public function __construct(){
        $this->config = \doujunyu\utility\common\Env::get('allin_pay');
//        $this->logIns = Log::getInstance();
//        $this->config = conf::getInstance();
//        $this->config->loadConf('./Config/config.php');
    }

    /**
     * [depositApply 充值申请]
     * 入金方式[收银宝快捷支付]
     */
    public function depositApply_QUICKPAY_VSP($bizUserId,$amount,$fee,$bankCardNo)
    {
        $yunClient = new yunClient();
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";
        $param["amount"] = $amount;
        $param["fee"] = $fee;
        $param["validateType"] = 0;
        $param["frontUrl"] = "https://www.baidu.com";
        $param["backUrl"] = "https://www.zhihu.com";
        $payParam["bankCardNo"] = $yunClient->RsaEncode($bankCardNo);
        $payParam["amount"] = $amount;
        $payMethod["QUICKPAY_VSP"] = $payParam;
        $param["payMethod"] = $payMethod;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 2;
        $serviceName = "OrderService";
        $motherName = "depositApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [depositApplyGateway 充值申请]
     * 入金方式[收银宝网关]
     */
    public function depositApply_GATEWAY_VSP($bizUserId,$amount,$fee)
    {

        $yunClient = new yunClient();
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";
        $param["amount"] = $amount;
        $param["fee"] = $fee;
        $param["validateType"] = 0;
        $param["frontUrl"] = "https://www.baidu.com";
        $param["backUrl"] = "https://www.zhihu.com";
        $payParam["amount"] = $amount;
        $payParam["paytype"] = "B2C,B2B";
        $payMethod["GATEWAY_VSP"] = $payParam;
        $param["payMethod"] = $payMethod;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 2;
        $serviceName = "OrderService";
        $motherName = "depositApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [createBankSubAcctNo 个人开户]
     */
    public function createBankSubAcctNo($bizUserId)
    {

        $yunClient = new yunClient();
        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";
        $param["acctOrgType"] = 1;
        $serviceName = "MemberService";
        $motherName = "createBankSubAcctNo";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [payBGCode 确认支付（后台+短信验证码确认）]
     * 收银宝快捷支付确认支付
     */
    public function payBGCode($bizUserId,$bizOrderNo,$verificationCode)
    {
        $param["bizUserId"] = $bizUserId;
        $param["bizOrderNo"] = $bizOrderNo;
        //$param["tradeNo"] = '{"sign":"","tphtrxcrtime":"","tphtrxid":0,"trxflag":"trx","trxsn":""}';
        $param["verificationCode"] = $verificationCode;
        $param["consumerIp"] = "10.168.1.55";
        $serviceName = "OrderService";
        $motherName = "pay";
        $yunClient = new yunClient();
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [payFront 确认支付（前台+短信验证码确认）]
     * 收银宝网关支付确认支付
     */
    public function payFront($bizUserId,$bizOrderNo)
    {
        $yunClient = new yunClient();
        $param["bizUserId"] = $bizUserId;
        $param["bizOrderNo"] = $bizOrderNo;
        $param["consumerIp"] = "10.168.1.55";
        $request["service"] = "OrderService";
        $request["method"] = "pay";
        $request["param"] = $param;
        $strRequest = json_encode($request);
        $strRequest = str_replace("\r\n", "", $strRequest);
        $ssoid = $this->config->getConf('sysid');
        $timestamp = date("Y-m-d H:i:s", time());
        $yunClient = new yunClient();
        $sign = $yunClient->sign($ssoid, $strRequest, $timestamp);
        $data = array();
        $data['sysid'] =$ssoid;
        $data['timestamp'] = $timestamp;
        $data['sign'] = $sign;
        $data['req'] = $strRequest;
        $sb = "";
        foreach ($data as $entry_key => $entry_value) {
            $sb .= $entry_key . '=' . urlencode($entry_value) . '&';
        }
        $sb = trim($sb, "&");
        $url = "http://116.228.64.55:6900/yungateway/frontTrans.do?".$sb;
        header("Location:$url");
    }

    /**
     * [withdrawApply 提现申请]
     * 支付方式默认为通联通代付
     * 提现方式默认为D0到账
     */
    public function withdrawApply($bizUserId,$amount,$fee,$bankCardNo)
    {
        $yunClient = new yunClient();
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";
        $param["amount"] = $amount;
        $param["fee"] = $fee;
        $param["validateType"] = 0;
        $param["backUrl"] = "https://www.zhihu.com";
        $param["bankCardNo"] = $yunClient->RsaEncode($bankCardNo);
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 2;
        $serviceName = "OrderService";
        $motherName = "withdrawApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }
    /**
     * [withdrawApply_HT 华通代付出金]
     */
    public function withdrawApply_HT()
    {
        $yunClient = new yunClient();
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["bizUserId"] = "4080";
        $param["accountSetNo"] = "200126";
        $param["amount"] = 30000;
        $param["validateType"] = 0;
        $param["backUrl"] = "https://www.zhihu.com";
        $param["bankCardNo"] = $yunClient->RsaEncode("6217004220016707113");
        //$param["bankCardPro"] = 1;
        $param["fee"] =286;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 2;
        $payparam["subAcctNo"] = "9120001000503110765";
        $payparam["PAYEE_ACCT_NO"] = "6217004220016707113";
        $payparam["PAYEE_ACCT_NAME"] = "西安智能科技有限公司";
        $payparam["AMOUNT"] = "30000";
        $payparam["SUMMARY"] = "提现";
        $Htarray = array();
        $Htarray['PAYEE_ACCT_NO'] = "6217004220016707113";
        $Htarray['PAYEE_ACCT_NAME'] = "西安智能科技有限公司";
        $Htarray['AMOUNT'] = "29714";
        $Htarray["SUMMARY"] = "提现";
        ksort($Htarray);
        $json = json_encode($Htarray,JSON_UNESCAPED_UNICODE);
        $payparam["SIGNED_MSG_MER"] = $yunClient->htBankWithdrawSign($json);
        $payMethod["WITHDRAW_HTBANK"] = $payparam;
        $param["payMethod"] = $payMethod;
        $serviceName = "OrderService";
        $motherName = "withdrawApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [consumeApply_BALANCE 消费申请]
     * 支付方式【账户余额】
     */
    public function consumeApply_BALANCE($bizUserId,$amount,$fee)
    {
        $yunClient = new yunClient();
        $param["payerId"] = $bizUserId;
        $param["recieverId"] = "#yunBizUserId_B2C#";
        $param["amount"] = $amount;
        $param["fee"] = $fee;
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["validateType"] = 0;
        $param["backUrl"] = "https://www.baidu.com";
        $payparam[0]["accountSetNo"]="200126";
        $payparam[0]["amount"]=$amount;
        $payMethod["BALANCE"] = $payparam;
        $param["payMethod"] = $payMethod;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 2;
        $serviceName = "OrderService";
        $motherName = "consumeApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [consumeApply_GATEWAY_VSP 消费申请]
     * 支付方式【收银宝网关】
     * 需要调用支付确认接口
     */
    public function consumeApply_GATEWAY_VSP($payerId,$recieverId,$amount,$fee)
    {
        $yunClient = new yunClient();
        $param["payerId"] = $payerId;
        $param["recieverId"] = $recieverId;
        $param["amount"] = $amount;
        $param["fee"] = $fee;
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["validateType"] = 0;
        $param["frontUrl"] = "https://www.zhihu.com";
        $param["backUrl"] = "https://www.baidu.com";
        $payparam["amount"] = 1;
        $payparam["paytype"] = "B2C";
        $payMethod["GATEWAY_VSP"] = $payparam;
        $param["payMethod"] = $payMethod;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 2;
        $serviceName = "OrderService";
        $motherName = "consumeApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [consumeApply_QUICKPAY_TLT 通联通协议支付]
     * 支付方式【通联通协议支付】
     * 只支持账户实名验证（四要素）、通联通协议支付签约方式绑定的银行卡
     * 支付确认调用“确认支付（后台+短信验证码确认）”/“确认支付【密码验证版】”接口
     */
    public function consumeApply_QUICKPAY_TLT($payerId,$recieverId,$amount,$fee,$bankCardNo)
    {
        $yunClient = new yunClient();
        $param["payerId"] = $payerId;
        $param["recieverId"] = $recieverId;
        $param["amount"] = $amount;
        $param["fee"] = $fee;
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["validateType"] = 0;
        $param["frontUrl"] = "https://www.zhihu.com";
        $param["backUrl"] = "https://www.baidu.com";
        $payparam["bankCardNo"] = $yunClient->RsaEncode($bankCardNo);
        $payparam["amount"] = $amount;
        $payMethod["QUICKPAY_TLT"] = $payparam;
        $param["payMethod"] = $payMethod;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 2;
        $serviceName = "OrderService";
        $motherName = "consumeApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [consumeApply_WECHAT_PUBLIC 微信JS支付]
     */
    public function consumeApply_WECHAT_PUBLIC($payerId,$recieverId,$amount,$fee)
    {
        $yunClient = new yunClient();
        $param["payerId"] = $payerId;
        $param["recieverId"] = $recieverId;
        $param["amount"] = $amount;
        $param["fee"] = $fee;
        $param["tradeCode"] = "3001";
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["validateType"] = 0;
        $param["backUrl"] = "https://www.baidu.com";
        $payparam["limitPay"] = "";
        $payparam["amount"] = $amount;
        $payparam["acct"] = "oIzed1G2EsljhLHUg4SQoxiPUQbY";
        $payMethod["WECHAT_PUBLIC"] = $payparam;
        $param["payMethod"] = $payMethod;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 1;
        $serviceName = "OrderService";
        $motherName = "consumeApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [consumeApply_ALIPAY_APP_OPEN description]
     */
    public function consumeApply_ALIPAY_APP_OPEN($payerId,$recieverId,$amount,$fee)
    {
        $yunClient = new yunClient();
        $param["payerId"] = "TLSXTESTUSER";
        $param["recieverId"] = "TLSXTEST0009";
        $param["amount"] = 1;
        $param["fee"] = 0;
        $param["tradeCode"] = "3001";
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["backUrl"] = "https://www.baidu.com";

        $payparam["amount"] = 1;
        $payMethod["ALIPAY_APP_OPEN"] = $payparam;
        $param["payMethod"] = $payMethod;
        $param["industryCode"] = "1910";
        $param["industryName"] = "其他";
        $param["source"] = 1;
        $serviceName = "OrderService";
        $motherName = "consumeApply";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [agentCollectApply 托管代收]
     */
    public function agentCollectApply($payData)
    {
        $yunClient = new YunClient();
        $param["bizOrderNo"] = "TLdemo".date("Ymdhis");//商户订单号（支付订单）
        $param["payerId"] = $payData['payerId'];//商户系统用户标识，商户系统中唯一编号。付款人

        //收款列表
        $paramList[0]["bizUserId"]=2;
        $paramList[0]["amount"]=$payData['amount'];
        $param["recieverList"] = $paramList;//收款列表
        $param["tradeCode"] = "3001";//业务码
        $param["amount"] = $payData['amount'];//订单金额
        $param["fee"] = 0;//手续费
        $param["backUrl"] = $this->config['BACK_URL'];//后台通知地址
        $param["validateType"] = 0;//交易验证方式

        $payparam["limitPay"]="";
        $payparam["amount"]=$payData['amount'];
        $payparam["acct"]='ojBRR5eV-b9hqtrMI-i8S2MJSzlA';
        $payMethod["WECHATPAY_MINIPROGRAM"] = $payparam;
        $param["payMethod"] = $payMethod;//支付方式

        $param["industryCode"] = "1910";//行业代码
        $param["industryName"] = "其他";//行业名称
        $param["source"] = 1;//访问终端类型

        $serviceName = "OrderService";
        $motherName = "agentCollectApply";
        return $yunClient->request($serviceName, $motherName, $param);

    }

    /**
     * [signalAgentPay 单笔托管代付]
     * 每笔单笔托管代付只支持一个收款人
     */
    public function signalAgentPay09($bizUserId,$bizOrderNo)
    {
        $yunClient = new yunClient();
        $param["bizOrderNo"] = "TL".date("Ymdhis");

        //付款列表
        $paramList[0]["bizOrderNo"]=$bizOrderNo;
        $paramList[0]["amount"]=1;
        $param["collectPayList"] = $paramList;

        // $splitRuleList[0]["bizUserId"]="TLSXTEST0010";
        // $splitRuleList[0]["amount"]=1;
        // $splitRuleList[0]["fee"]=0;
        // $param["splitRuleList"] = $splitRuleList;

        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";

        $param["tradeCode"] = "4001";
        $param["amount"] = 1;
        $param["fee"] = 0;
        $param["backUrl"] = "https://www.baidu.com";

        $serviceName = "OrderService";
        $motherName = "signalAgentPay";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [signalAgentPay 单笔托管代付]
     */
    public function signalAgentPay10($bizUserId,$bizOrderNo)
    {
        $yunClient = new yunClient();
        $param["bizOrderNo"] = "yunno".date("Ymdhis");
        //付款列表
        $paramList[0]["bizOrderNo"]=$bizOrderNo;
        $paramList[0]["amount"]=1;
        $param["collectPayList"] = $paramList;
        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";

        $param["tradeCode"] = "4001";
        $param["amount"] = 1;
        $param["fee"] = 0;
        $param["backUrl"] = "https://www.baidu.com";

        $serviceName = "OrderService";
        $motherName = "signalAgentPay";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [refund 退款申请]
     * 支持充值、消费、托管代收（未代付、已代付）、平台转账订单发起退款。
     * 支持个人会员、企业会员相关订单的退款，不支持平台自身发起订单的退款。
     */
    public function refund($bizUserId,$oriBizOrderNo)
    {
        $yunClient = new yunClient();
        $param["bizOrderNo"] = "TL".date("Ymdhis");
        $param["oriBizOrderNo"] = $oriBizOrderNo;
        $param["bizUserId"] = $bizUserId;
        $param["refundType"] = "D0";
        //托管代收订单中的收款人的退款金额
        $paramList[0]["bizUserId"]="TLSXTEST0009";
        $paramList[0]["amount"]=1;
        $paramList[0]["accountSetNo"]="200126";
        $paramList[1]["bizUserId"]="TLSXTEST0010";
        $paramList[1]["amount"]=1;
        $paramList[1]["accountSetNo"]="200126";
        $param["refundList"] = $paramList;
        $param["amount"] = 2;
        $param["backUrl"] = "https://www.baidu.com";
        $serviceName = "OrderService";
        $motherName = "refund";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [applicationTransfer 平台转账]
     * 目前只支持从平台标准余额账户集、保证金账户、营销专用账户、预付卡账户集和自定义 A 帐户转账到用户托管账户余额。
     * 源帐户集：标准余额账户集、平台保证金账户、营销专用账户、预付卡账户集和自定义 A 帐户所属的账户集。
     * 目标账户集：通商云分配给业务端的托管专用账户集。
     */
    public function applicationTransfer($targetBizUserId,$amount)
    {
        $yunClient = new yunClient();
        $param["bizTransferNo"] = "TL".date("Ymdhis");
        $param["sourceAccountSetNo"] = "100001";
        $param["targetBizUserId"] = $targetBizUserId;
        $param["targetAccountSetNo"] = "200126";
        $param["amount"] = $amount;
        $param["backUrl"] = "https://www.baidu.com";
        $serviceName = "OrderService";
        $motherName = "applicationTransfer";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [queryBalance 查询余额]
     */
    public function queryBalance($bizUserId)
    {
        $yunClient = new yunClient();
        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";

        $serviceName = "OrderService";
        $motherName = "queryBalance";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [getOrderDetail 查询订单状态]
     */
    public function getOrderDetail($bizOrderNo)
    {
        $yunClient = new yunClient();
        $param["bizOrderNo"] = $bizOrderNo;
        $serviceName = "OrderService";
        $motherName = "getOrderDetail";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [queryInExpDetail 查询账户收支明细]
     */
    public function queryInExpDetail($bizUserId)
    {
        $timestart='2019-8-13';
        $timestamp = strtotime($timestart);
        $dateStart = date("Y-m-d",$timestamp);
        $timeend='2019-8-14';
        $timestamp = strtotime($timeend);
        $dateEnd = date("Y-m-d",$timestamp);
        $yunClient = new yunClient();
        $param["bizUserId"] = $bizUserId;
        $param["accountSetNo"] = "200126";
        $param["dateStart"] = $dateStart;
        $param["dateEnd"] = $dateEnd;
        $param["startPosition"] = 1;
        $param["queryNum"] = 5;
        $serviceName = "OrderService";
        $motherName = "queryInExpDetail";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [getPaymentInformationDetail description]
     */
    public function getPaymentInformationDetail($bizOrderNo)
    {
        $timestart='2019-8-13';
        $timestamp = strtotime($timestart);
        $dateStart = date("Y-m-d",$timestamp);

        $timeend='2019-8-14';
        $timestamp = strtotime($timeend);
        $dateEnd = date("Y-m-d",$timestamp);

        $yunClient = new yunClient();
        $param["bizOrderNo"] = $bizOrderNo;
        $param["accountSetNo"] = "200126";
        $param["dateStart"] = $dateStart;
        $param["dateEnd"] = $dateEnd;
        $serviceName = "OrderService";
        $motherName = "getPaymentInformationDetail";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [getPayeeFundsInTransitDetail 收款方在途资金明细查询]
     */
    public function getPayeeFundsInTransitDetail($bizUserId,$bizOrderNo)
    {
        $timestart='2019-8-13';
        $timestamp = strtotime($timestart);
        $dateStart = date("Y-m-d",$timestamp);

        $timeend='2019-8-14';
        $timestamp = strtotime($timeend);
        $dateEnd = date("Y-m-d",$timestamp);

        $yunClient = new yunClient();
        $param["bizUserId"] = $bizUserId;
        $param["bizOrderNo"] = $bizOrderNo;//托管代收订单号
        $param["accountSetNo"] = "200126";
        $param["dateStart"] = $dateStart;
        $param["dateEnd"] = $dateEnd;
        $serviceName = "OrderService";
        $motherName = "getPaymentInformationDetail";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [queryReserveFundBalance 通联通头寸查询]
     */
    public function queryReserveFundBalance()
    {
        $yunClient = new yunClient();
        $param["sysid"] =  $this->config->getConf('sysid');
        $serviceName = "MerchantService";
        $motherName = "queryReserveFundBalance";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [getCheckAccountFile 平台集合对账下载]
     */
    public function getCheckAccountFile()
    {
        $yunClient = new yunClient();
        $timestart='2019-8-13';
        $timestamp = strtotime($timestart);
        $dateStart = date("Ymd",$timestamp);
        $param["date"] = $dateStart;
        $serviceName = "MerchantService";
        $motherName = "getCheckAccountFile";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

    /**
     * [queryMerchantBalance 平台账户集余额查询]
     * 支持查询平台在通商云系统中各账户集余额
     */
    public function queryMerchantBalance()
    {
        $yunClient = new yunClient();
        $param["accountSetNo"] = "100001";
        $serviceName = "MerchantService";
        $motherName = "queryMerchantBalance";
        $result = $yunClient->request($serviceName, $motherName, $param);
        var_dump($result);
    }

}
