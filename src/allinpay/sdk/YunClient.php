<?php
namespace doujunyu\utility\allinpay\sdk;

class YunClient
{


    private  $config;
    private  $priKey = null;
    public function __construct(){
        $this->config = \doujunyu\utility\common\Env::get('allin_pay');
    }

    /**
     *请求封装
     */
    public function request($service, $method, $param)
    {
        $ssoid = $this->config['SYS_ID'];  //商户ID
        $request["service"] = $service;
        $request["method"] = $method;
        $request["param"] = $param;
        $strRequest = json_encode($request,JSON_UNESCAPED_UNICODE);
        $strRequest = str_replace("\r\n", "", $strRequest);
        $req['req'] = $strRequest;
        $req['sysid'] = $ssoid;
        $timestamp = date("Y-m-d H:i:s", time());
        $sign = $this->sign($ssoid, $strRequest, $timestamp);
        $req['timestamp'] = $timestamp;
        $req['sign'] = $sign;
        $req['v'] = $this->config['VERSION'];
        $serverAddress =$this->config['SERVER_URL'];
        $result = $this->requestYSTAPI($serverAddress, $req);
        return $this->checkResult($result);
    }

    /**
     *对数据进行签名
     */
    public function sign($ssoid, $strRequest, $timestamp)
    {
        $dataStr = $ssoid . $strRequest . $timestamp;
        $text = base64_encode(hash('md5', $dataStr, true));
        $privateKey = $this->getPrivateKey();
        openssl_sign($text, $sign, $privateKey);
        @openssl_free_key($privateKey);
        $sign = base64_encode($sign);
        return $sign;
    }

    /**
     *获取私匙的绝对路径;
     */
    public function getPrivateKey()
    {
        return $this->loadPrivateKey(dirname(__DIR__, 6).$this->config['PATH'],$this->config['PWD']);
    }

    /**
     * 从证书文件中装入私钥 pem格式;
     */
    private function loadPrivateKey($path, $pwd)
    {
        $str = explode('.', $path);
        $houzuiName = $str[count($str) - 1];
        if ($houzuiName == "pfx") {
            return $this->loadPrivateKeyByPfx($path, $pwd);
            $priKey = file_get_contents($path);
            $res = openssl_get_privatekey($priKey, $pwd);
            return $res;
        }
        if ($houzuiName == "pem") {
            $priKey = file_get_contents($path);
            $res = openssl_get_privatekey($priKey, $pwd);
            if (!$res) {
                exit('您使用的私钥格式错误，请检查私钥配置');
            }
            return $res;
        }
    }

    /**
     * 从证书文件中装入私钥 Pfx 文件格式
     */
    private function loadPrivateKeyByPfx($path, $pwd)
    {
        if (file_exists($path)) {
            $priKey = file_get_contents($path);
            if (openssl_pkcs12_read($priKey, $certs, $pwd)) {
                $privateKey = $certs['pkey'];
                return $privateKey;
            }
            die("私钥文件格式错误");
        }
        die('私钥文件不存在');
    }

    /**
     *请求云商通URL
     */
    private function requestYSTAPI($serverUrl, $args)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serverUrl);
        $sb = '';
        $reqbody = array();
        foreach ($args as $entry_key => $entry_value) {
            $sb .= $entry_key . '=' . urlencode($entry_value) . '&';
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sb);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-length', count($reqbody)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $result;
    }

    /**
     *检查返回的结果是否合法;
     */
    private function checkResult($result)
    {

        $arr = json_decode($result, true);
        $sign = $arr['sign'];
        $signedValue = $arr['signedValue'];

        $success = false;
        if ($sign != null) {
            $success = $this->verify($this->getPublicKeyPath(), $signedValue, $sign);
        }
        if ($success) {
            return $arr;
        }
        return $success;
    }

    /**
     *验证返回的数据的合法性
     */
    private function verify($publicKeyPath, $signedValue, $sign)
    {
        $signedValue = base64_encode(hash('md5', $signedValue, true));
        $pubKeyId = openssl_get_publickey(file_get_contents($publicKeyPath));
        $result = (bool)openssl_verify($signedValue, base64_decode(trim($sign)), $pubKeyId, OPENSSL_ALGO_SHA1);
        openssl_free_key($pubKeyId);
        return $result;
    }

    /**
     * [RsaEncode 隐私数据加密]
     */
    public function RsaEncode($data)
    {
        $publicKeyPath = $this->getPublicKeyPath();
        $publicKey = openssl_get_publickey(file_get_contents($publicKeyPath));
        $encrypted = "";
        openssl_public_encrypt($data,$encrypted,$publicKey);
        return strtoupper(bin2hex($encrypted));
    }

    /**
     * [RsaDecode 隐私数据解密]
     */
    public function RsaDecode($data)
    {
        $data = $this->Hex2String($data);
        $prikey = $this->getPrivateKey();
        if(!openssl_private_decrypt($data, $decrypt_data, $prikey,OPENSSL_PKCS1_PADDING))
        {
            echo "<br/>ErrorMsg====" . openssl_error_string() . "<br/>";
        }
        return $decrypt_data;
    }

    public function Hex2String($hex){
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }

    /**
     *获取公匙的绝对路径
     */
    public function getPublicKeyPath()
    {
        return dirname(__DIR__, 6).$this->config['TL_CERT_PATH'];
    }

    /**
     *华通银行签名
     * 提现方式withdrawType为华通代付WITHDRAW_HTBANK，需上送SIGNED_MSG_MER，用华通分配给商户的私钥签名
     * @param $text 取收款人账号、收款人户名、金额、摘要字段按ASCII码升序后的json字符串(参考接口文档说明)
     * @return $sign
     */
    public function htBankWithdrawSign($text){
        $htCertPath = $this->config->getConf('htCertPath');
        $htCertPwd = $this->config->getConf('htCertPwd');
        $htPriKey = $this->loadPrivateKeyByPfx($htCertPath,$htCertPwd);
        openssl_sign($text, $sign, $htPriKey);
        openssl_free_key($htPriKey);
        $sign = base64_encode($sign);
        return $sign;
    }

}
