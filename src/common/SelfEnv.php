<?php

namespace doujunyu\utility\common;


/** post 请求
 * Class SerCurl
 * @package App\Service
 */
class SelfEnv
{
    /**
     * 获取环境变量值
     * @access public
     * @param string $name 环境变量名（支持二级 . 号分割）
     * @param string $default 默认值
     * @return mixed
     */
    public static function get(string $name, $default = null)
    {
        //根据句号进行拆分
        $name = strtoupper($name.'.');
        [$server, $key] = explode('.', $name);
        //获取.env里面的值
        $ini_data = parse_ini_file(dirname(__DIR__, 5) . '/.env',true);
        return $ini_data[$server][$key] ?? ($ini_data[$server] ?? $default);
    }
}