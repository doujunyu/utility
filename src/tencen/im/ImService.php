<?php
namespace doujunyu\utility\tencen\im;

use function GuzzleHttp\json_decode;
use doujunyu\utility\tencen\im\package\TLSSigAPIv2;


class ImService
{
    //腾讯即时通讯
    protected $im_url = "https://console.tim.qq.com/";
    protected $sdkappid = '';
    protected $key = '';
    protected $identifier = '';
    protected $prefix = '';
    public $is_start = true; //是否开启即时通讯：false 关闭，true =开启
    public function __construct($sdkappid,$key,$identifier,$prefix)
    {
        $this->sdkappid = $sdkappid;
        $this->key = $key;
        $this->identifier = $identifier;
        $this->prefix = $prefix;
    }

    /**
     * Notes:获取公众参数
     * User: zws
     * Date: 2021/10/27
     * Time: 下午 4:09
     */
    public function get_commen_param($uid){
//        $uid = $this->prefix.$uid;
        $data = [
            'sdkappid'=>$this->sdkappid,
            'identifier'=>$this->identifier,
            'usersig'=>$this->usersig($uid),
            'random'=>mt_rand(100000,999999),
            'contenttype'=>'json'
        ];
        return http_build_query($data);
    }

    /**
     * Notes:生成im密码
     * User: zws
     * Date: 2021/10/27
     * Time: 下午 4:09
     */
    public function usersig($uid){
//        $uid = $this->prefix.$uid;
        $service = new TLSSigAPIv2($this->sdkappid, $this->key);
        return $service->genUserSig($uid);
    }

    /**
     * Notes:导入单个账号
     * User: zws
     * Date: 2021/10/27
     * Time: 下午 4:12
     */
    public function account_import($uid, $avatar, $nickname){
        
        $uid = $this->prefix.$uid;
        $data = [
            'Identifier'=>"$uid",
            'Nick'=>$nickname,
            'FaceUrl'=>$avatar
        ];
        $url = $this->im_url.'v4/im_open_login_svc/account_import?'.$this->get_commen_param($this->identifier);
        $res = self::post_http($url, $data);
        return $res;
    }

    /**
     * Notes:设置im资料
     * User: zws
     * Date: 2021/10/27
     * Time: 下午 4:38
     */
    public function portrait_set($uid, $nickname, $avatar){
        $uid = $this->prefix.$uid;
        $ProfileItem = [];
        if($nickname){
            $ProfileItem[] =   [
                'Tag'=>'Tag_Profile_IM_Nick',
                'Value'=>$nickname,
            ];
        }
        if($avatar){
            $ProfileItem[] =   [
                'Tag'=>'Tag_Profile_IM_Image',
                'Value'=>$avatar,
            ];
        }
        $data = [
            'From_Account'=>"$uid",
            'ProfileItem'=>$ProfileItem,
        ];
        $url = $this->im_url.'v4/profile/portrait_set?'.$this->get_commen_param($this->identifier);
        $res = self::post_http($url, $data);
        return $res;
//        if(isset($res['ActionStatus']) && $res['ActionStatus'] == 'OK'){
//            return true;
//        }
//        return false;
    }

    public function getInfo(){

        $data = [
            'To_Account'=>["xj4"],
            'TagList'=>["Tag_Profile_IM_Nick"],
        ];
        $url = $this->im_url.'v4/profile/portrait_get?'.$this->get_commen_param($this->identifier);
        $res = self::post_http($url, $data);
        var_dump($res);die;
        if(isset($res['ActionStatus']) && $res['ActionStatus'] == 'OK'){
            return true;
        }
        return false;
    }

    public function delAccount($deleteDatas){
        $DeleteItem = [];
        if($deleteDatas){
            foreach ($deleteDatas as $v){
                $DeleteItem[] = [
                    'UserID' => $this->prefix.$v
                ];
            }
        }
        $data = [
            'DeleteItem'=>$DeleteItem
//                [
////                ['UserID'=>"11"],
////                ['UserID'=>"12"],
////                ['UserID'=>"13"],
////                ['UserID'=>"14"],
////                ['UserID'=>"15"],
////                ['UserID'=>"16"],
////                ['UserID'=>"17"],
////                "11","12","13","14","15","16","17"
//            ],
        ];
        $url = $this->im_url.'v4/im_open_login_svc/account_delete?'.$this->get_commen_param($this->identifier);
        $res = self::post_http($url, $data);
        var_dump($res);
    }

    /**
     * 批量发单聊消息
     */
    public function batchsendmsg($send_user_id, $receive_user_ids, $message_datas){
        $To_Account = [];
        foreach ($receive_user_ids as $v){
            $To_Account[] = $this->prefix.$v;
        }
        $MsgBody = [];
        foreach ($message_datas as $v){
            $MsgBody[] = [
                'MsgType' => 'TIMCustomElem',
                'MsgContent' => [
        //                    'Desc' => '您有一条新的消息'
                    'Ext' => json_encode([
                        'dataType' => $v['type'],
                        'fileSize' => $v['filesize'],
                        'fileName' => $v['filename'],
                    ]),
                    'Data' => $v['message'],
                ]
            ];
        }
        $data = [
            'SyncOtherMachine' => 1,//1：把消息同步到 From_Account 在线终端和漫游上 ,2不同步
            'From_Account' => $this->prefix.$send_user_id,
            'To_Account' => $To_Account,
            'MsgRandom' => rand(10000, 99999),
            'MsgBody' => $MsgBody
        ];
        $url = $this->im_url.'v4/openim/batchsendmsg?'.$this->get_commen_param($this->identifier);
        $res = self::post_http($url, $data);
        if(isset($res['ActionStatus']) && $res['ActionStatus'] == 'OK'){
            return true;
        }
        return false;
    }

    public function getroammsg($To_Account, $MaxCnt = 100, $From_Account, $MinTime, $MaxTime, $LastMsgKey = '')
    {
        if($LastMsgKey)
        {
            $params = [
                "From_Account"=> $this->prefix.$From_Account,
                "To_Account"=> $this->prefix.$To_Account,
                "MaxCnt" => $MaxCnt,
                "MinTime" => $MinTime,
                "MaxTime" => $MaxTime,
                "LastMsgKey" => $LastMsgKey
            ];
        }
        else
        {
            $params = [
                "From_Account"=> $this->prefix.$From_Account,
                "To_Account"=> $this->prefix.$To_Account,
                "MaxCnt" => $MaxCnt,
                "MinTime" => $MinTime,
                "MaxTime" => $MaxTime,
            ];
        }

        $url = $this->im_url.'v4/openim/admin_getroammsg?'.$this->get_commen_param($this->identifier);
        $res = self::post_http($url, $params);

        $res = self::post_http($url, $params);

        return $res;
    }

    protected static function post_http($url,$data){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data,JSON_UNESCAPED_UNICODE));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result, true);
        return $res;
    }

}
