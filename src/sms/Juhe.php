<?php
namespace doujunyu\utility\sms;
class Juhe
{
    //参数配置
    const API_URL = 'http://v.juhe.cn/sms/send';//创蓝变量短信接口URL

    private  $key;

    public function __construct($key){
        $this->key = $key;
    }

    /**
     * @param string $mobile 手机号
     * @param int $tpl_id 模板ID
     * @param array $vars 数据
     * @return string 返回json字符串
     */
    public function send(string $mobile,int $tpl_id,array $vars):string
    {
        $vars_to_json = json_encode($vars);
        $url = self::API_URL."?mobile={$mobile}&tpl_id={$tpl_id}&vars={$vars_to_json}&key={$this->key}";
        return file_get_contents($url);
    }

}



