<?php
// +----------------------------------------------------------------------
// | HisiPHP框架[基于ThinkPHP5.1开发]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2021 http://www.hisiphp.com
// +----------------------------------------------------------------------
// | HisiPHP承诺基础框架永久免费开源，您可用于学习和商用，但必须保留软件版权信息。
// +----------------------------------------------------------------------
// | Author: 橘子俊 <364666827@qq.com>，开发者QQ群：50304283
// +----------------------------------------------------------------------
namespace doujunyu\utility\pay\ali;
require '../extend/Pay/AliPay/sdk/aop/AopClient.php';
require '../extend/Pay/AliPay/sdk/aop/AopCertification.php';
require '../extend/Pay/AliPay/sdk/aop/request/AlipayTradeQueryRequest.php';
require '../extend/Pay/AliPay/sdk/aop/request/AlipayTradeWapPayRequest.php';
require '../extend/Pay/AliPay/sdk/aop/request/AlipayTradeAppPayRequest.php';

class AppPay
{
    // 自动写入时间戳
    public static function index($data,$pay_key){
        if(empty($data)){
            return '空';
        }
        $aop = new \AopClient();
        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
        $aop->appId = $pay_key['appId'];
        $aop->rsaPrivateKey = $pay_key['rsaPrivateKey'];
        $aop->format = "json";
        $aop->charset = "UTF-8";
        $aop->signType = "RSA2";
        $aop->alipayrsaPublicKey = $pay_key['alipayrsaPublicKey'];
//实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        $request = new \AlipayTradeAppPayRequest();
//SDK已经封装掉了公共参数，这里只需要传入业务参数
        $bizcontent = json_encode($data);
        $request->setNotifyUrl($pay_key['backUrl']);
        $request->setBizContent($bizcontent);
//这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
//htmlspecialchars是为了输出到页面时防止被浏览器将关键参数html转义，实际打印到日志以及http传输不会有这个问题
        return htmlspecialchars($response);//就是orderString 可以直接给客户端请求，无需再做处理。
    }
    //回调验证
    public static function back($data){
        $pay_key = \think\facade\Config::get('account.aliPay');
        $aop = new \AopClient();
        $aop->alipayrsaPublicKey=$pay_key['alipayrsaPublicKey'];
        $check = $aop->rsaCheckV2($data,$pay_key['alipayrsaPublicKey'],'RSA2');
        if($check){
            return true;
        }else{
            return false;
        }

    }

}
