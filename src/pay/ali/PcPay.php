<?php
namespace doujunyu\utility\pay\ali;
use AlipayTradePagePayContentBuilder;
use AlipayTradeService;


require_once dirname(__FILE__).'/sdk/pagepay/service/AlipayTradeService.php';
require_once dirname(__FILE__).'/sdk/pagepay/buildermodel/AlipayTradePagePayContentBuilder.php';

class PcPay{
    private $app_id = "appid";
    private $merchant_private_key = "秘钥";
    private $notify_url = "http://外网可访问网关地址/alipay.trade.page.pay-PHP-UTF-8/notify_url.php";
    private $return_url = "http://外网可访问网关地址/alipay.trade.page.pay-PHP-UTF-8/return_url.php";
    private $charset = "UTF-8";
    private $sign_type = "RSA2";
    private $gatewayUrl = "https://openapi.alipay.com/gateway.do";
    private $alipay_public_key = "支付宝公钥";


    public function __construct($app_id,$merchant_private_key,$alipay_public_key,$notify_url,$return_url = '') {
        $this->app_id = $app_id;//应用ID,您的APPID。
        $this->merchant_private_key = $merchant_private_key;//商户私钥
        $this->notify_url = $notify_url;//异步通知地址
        $this->return_url = $return_url;//同步跳转
        $this->alipay_public_key = $alipay_public_key;//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    }

    public function index($order,$order_name,$price,$content = '空'){
        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = trim($order);
        //订单名称，必填
        $subject = trim($order_name);
        //付款金额，必填
        $total_amount = trim($price);
        //商品描述，可空
        $body = trim($content);
        //构造参数
        $payRequestBuilder = new AlipayTradePagePayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setOutTradeNo($out_trade_no);
        $aop = new AlipayTradeService([
            'app_id'=>$this->app_id,
            'merchant_private_key'=>$this->merchant_private_key,
            'notify_url'=>$this->notify_url,
            'return_url'=>$this->return_url,
            'charset'=>$this->charset,
            'sign_type'=>$this->sign_type,
            'gatewayUrl'=>$this->gatewayUrl,
            'alipay_public_key'=>$this->alipay_public_key
        ]);

        /**
         * pagePay 电脑网站支付请求
         * @param $builder 业务参数，使用buildmodel中的对象生成。
         * @param $return_url 同步跳转地址，公网可以访问
         * @param $notify_url 异步通知地址，公网可以访问
         * @return $response 支付宝返回的信息
         */
        $response = $aop->pagePay($payRequestBuilder,$this->return_url, $this->notify_url );
        //输出表单
        return $response;
    }

    //回调验证
    public function back($data){
                $alipaySevice = new AlipayTradeService([
            'gatewayUrl'=>$this->gatewayUrl,
            'app_id'=>$this->app_id,
            'merchant_private_key'=>$this->merchant_private_key,
            'alipay_public_key'=>$this->alipay_public_key,
            'charset'=>$this->charset,
            'sign_type'=>$this->sign_type,
        ]);
        $alipaySevice->writeLog(var_export($_POST,true));
        $result = $alipaySevice->check($data);
        return $result;
    }
}
















