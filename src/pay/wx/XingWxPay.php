<?php


namespace doujunyu\utility\pay\wx;

class XingWxPay
{
    private $appid = 0;
    private $mch_id = 0;
    private $key = 0;
    private $backUrl = 0;
    public function __construct($appid ,$mch_id,$key,$backUrl) {

        $this->appid = $appid;
        $this->mch_id = $mch_id;
        $this->key = $key;
        $this->backUrl = $backUrl;
    }

    //统一下单
    public function WeChatPay($no,$money,$body = '空')
    {
        $time = time();//时间

        $appid = $this->appid;             //微信开放平台appID
        $mch_id = $this->mch_id;           //微信支付商户号 MCHID
        $wx_api_key = $this->key;   //微信支付密钥 APIKEY
        $out_trade_no = $no;            //自己业务系统生成的交易no，可以唯一标识
        $client_ip = get_client_ip();            //客户端ip
        $notify_url = $this->backUrl;     //接收支付结果通知url
        $nonce_str = $this->randomStr(32);        //随机20位字符串
        $total_fee = $money*100;           //金额


        $data = array();
        $data['appid'] = $appid;
        $data['mch_id'] = $mch_id;
        $data['nonce_str'] = $nonce_str;
        $data['body'] = $body;
        $data['detail'] = "App支付";
        $data['out_trade_no'] = $out_trade_no;
        $data['total_fee'] = $total_fee;  //注意 单位是分
        $data['spbill_create_ip'] = $client_ip;
        $data['notify_url'] = $notify_url;
        $data['trade_type'] = "APP";  //交易类型

        // 生成签名
        $data['sign'] = $this->makeSign($data, $wx_api_key);

        // 请求API
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';//下单地址
        $result = $this->postXmlCurl($this->toXml($data), $url);//11

        $prepay = $this->fromXml($result);
        // 请求失败

            if ($prepay['return_code'] === 'FAIL') {
                return ['code'=>0,'data'=>$prepay];
            }
            if ($prepay['result_code'] === 'FAIL') {
                return ['code'=>0,'data'=>$prepay];
//                die(['msg' => $prepay['return_msg'], 'code' => -10]);
            }

        $second["appid"] = $appid;
        $second["noncestr"] = $data['nonce_str'];
        $second["package"] = "Sign=WXPay";
        $second["partnerid"] = $mch_id;
        $second["prepayid"] = $prepay['prepay_id'];
        $second["timestamp"] = time();
        $s = $this->makeSign($second, $wx_api_key);
        $second["sign"] = $s;
//        return $data;
        // 生成 nonce_str 供前端使用
//        $second = array();
//        $second['appid'] = $appid;
//        $second['partnerid'] = $mch_id;
//        $second['prepayid'] = $prepay['prepay_id'];
//        $second['package'] = "Sign=WXPay";
//        $second['noncestr'] = $data['nonce_str'];
//        $second['timestamp'] = $time;
//        $second['sign'] = $this->makeSign($second, $wx_api_key);
//        return ['code'=>1,'data'=>$second];
//        $paySign = $this->makePaySign($data['nonce_str'], $prepay['prepay_id'], $time, $wx_api_key);
        return ['code'=>1,'data'=>$second];

    }

    /**
     * 生成签名
     * @param $values
     * @return string 本函数不覆盖sign成员变量，如要设置签名需要调用SetSign方法赋值
     */
    private function makeSign($values, $wx_api_key)
    {
        //签名步骤一：按字典序排序参数
        ksort($values);
        $string = $this->toUrlParams($values);
        //签名步骤二：在string后加入KEY
        $string = $string . "&key=" . $wx_api_key;
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }
    /**
     * 格式化参数格式化成url参数
     * @param $values
     * @return string
     */
    private function toUrlParams($values)
    {
        $buff = '';
        foreach ($values as $k => $v) {
            if ($k != 'sign' && $v != '' && !is_array($v)) {
                $buff .= $k . '=' . $v . '&';
            }
        }
        return trim($buff, '&');
    }
    /**
     * 以post方式提交xml到对应的接口url
     * @param $xml
     * @param $url
     * @param int $second
     * @return mixed
     */
    private function postXmlCurl($xml, $url, $second = 30)
    {
        $ch = curl_init();
        // 设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);//严格校验
        // 设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        // 要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        // 运行curl
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    /**
     * 输出xml字符
     * @param $values
     * @return bool|string
     */
    private function toXml($values)
    {
        if (!is_array($values)
            || count($values) <= 0
        ) {
            return false;
        }

        $xml = "<xml>";
        foreach ($values as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }
    /**
     * 将xml转为array
     * @param $xml
     * @return mixed
     */
    private function fromXml($xml)
    {
        // 禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    }
    /**
     * 生成paySign
     * @param $nonceStr
     * @param $prepay_id
     * @param $timeStamp
     * @return string
     */
    private function makePaySign($nonceStr, $prepay_id, $timeStamp, $wx_api_key)
    {
        $data = [
            'appId' => config("web.appid"),
            'partnerId' => config("web.mch_id"),
            'nonceStr' => $nonceStr,
            'package'  => 'prepay_id=' . $prepay_id,
            'signType' => 'MD5',
            'timeStamp' => $timeStamp,
        ];

        //签名步骤一：按字典序排序参数
        ksort($data);

        $string = $this->toUrlParams($data);

        //签名步骤二：在string后加入KEY
        $string = $string . '&key=' . $wx_api_key;

        //签名步骤三：MD5加密
        $string = md5($string);

        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);

        return $result;
    }

    /**
     * 生成指定长度的随机字符串(包含大写英文字母, 小写英文字母, 数字)
     * @param $length int 需要生成的字符串的长度
     * @return string 包含 大小写英文字母 和 数字 的随机字符串
     */
    public function randomStr($length)
    {
        //生成一个包含 大写英文字母, 小写英文字母, 数字 的数组
        $arr = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        $str = '';
        $arr_len = count($arr);
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $arr_len - 1);
            $str .= $arr[$rand];
        }
        return $str;
    }
    /**
     * 支付成功异步通知
     * @param \app\model\Order
     */
    public function notify(Request $request)
    {
        if (!$xml = file_get_contents('php://input')) {
            $this->returnCode(false, 'Not found DATA');
        }
        file_put_contents('./log.txt',$xml,FILE_APPEND | LOCK_EX);
        // 将服务器返回的XML数据转化为数组
        $data = $this->fromXml($xml);
        // 记录日志
//        $dir = public_path('paylog/');
//        $this->write_log($xml, $dir);
//        $this->write_log($data, $dir);
        // 订单信息

        $order = Order::where('no', $data['out_trade_no'])->first();
        empty($order) && $this->returnCode(true, '订单不存在');


        // 保存微信服务器返回的签名sign
        $dataSign = $data['sign'];
        // sign不参与签名算法
        unset($data['sign']);
        // 生成签名
        $sign = $this->makeSign($data,config("web.wx_api_key"));
        // 判断签名是否正确  判断支付状态
        if (($sign === $dataSign)
            && ($data['return_code'] == 'SUCCESS')
            && ($data['result_code'] == 'SUCCESS')) {
            // 更新订单状态
            $dataorder['result'] = $xml;//返回数据
//            $dataorder['type'] = 1';//支付方式
            $dataorder['return'] = $data['transaction_id'];//返回提示
            $dataorder['status'] = 2;//成功
            Order::where('Id', $order->Id)->update($dataorder);
            Member::where('id',$order->uid)->increment('gold',$order->money);
            // 返回状态
            $this->returnCode(true, 'OK');
        }
        // 返回状态
        $this->returnCode(false, '签名失败');
    }

    /**
     * 返回状态给微信服务器
     * @param bool $is_success
     * @param string $msg
     */
    private function returnCode($is_success = true, $msg = null)
    {
        $xml_post = $this->toXml([
            'return_code' => $is_success ? $msg ?: 'SUCCESS' : 'FAIL',
            'return_msg' => $is_success ? 'OK' : $msg,
        ]);
        die($xml_post);
    }

}