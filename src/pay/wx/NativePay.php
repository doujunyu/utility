<?php

namespace doujunyu\utility\pay\wx;


require '../extend/Pay/WxPay/sdk/lib/WxPay.Api.php';
require '../extend/Pay/WxPay/sdk/example/phpqrcode/phpqrcode.php';
class NativePay
{
    # 接口API URL前缀
    const API_URL_PREFIX = 'https://api.mch.weixin.qq.com';
    # 查询订单
    const ORDER_QUERY = '/pay/orderquery';
    # 下单地址URL
    const UNIFIEDORDER_URL = "/pay/unifiedorder";
    # 应用APPID
    private $appid;
    # 商户号
    private $mch_id;
    # 终端设备号
    private $device_info = 'WEB';
    # 随机字符串
    private $nonce_str;
    # 签名
    private $sign;
    # 签名类型
    private $sign_type = 'MD5';
    # 商品描述
    private $body;
    # 商品详情
    private $detail;
    # 附加数据
    private $attach = 'QUICK_MSECURITY_PAY';
    # 商户订单号
    private $out_trade_no;
    # 货币类型
    private $fee_type = 'CNY';
    # 总金额
    private $total_fee;
    # 终端IP
    private $spbill_create_ip;
    # 交易开始时间
    private $time_start;
    # 交易结束时间
    private $time_expire;
    # 订单优惠标记
    private $goods_tag;
    # 回调通知地址
    private $notify_url;
    # 支付类型
    private $trade_type = 'NATIVE';
//    # 指定支付方式 no_credit 指定不能使用信用卡支付
    private $limit_pay;

    # 支付密钥
    private $key;
    # 返回参数
    private $params = array();

    # 微信支付信息
    public function __construct($appid,$mch_id,$key,$notify_url)
    {
        $this->appid      = $appid;
        $this->mch_id     = $mch_id;
        $this->notify_url = $notify_url;
        $this->key        = $key;
    }

    # 微信支付订单调用
    public function index($params = [])
    {
        if(!$params)return false;
        $this->body                  = $params['body'];
        $this->out_trade_no          = $params['number'];
        $this->total_fee             = intval(bcmul($params['price'],100));
        $this->nonce_str             = $this->getRandStr();
        $this->time_start            = date('YmdHis',time());
        $this->time_expire           = date('YmdHis',(time()+60*60*2));
//        $this->spbill_create_ip      = $_SERVER['REMOTE_ADDR'];
        $this->spbill_create_ip      = $params['ip'];
        $this->params['appid']            = $this->appid;
        $this->params['mch_id']           = $this->mch_id;
        $this->params['device_info']      = $this->device_info;
        $this->params['nonce_str']        = $this->nonce_str;
        $this->params['sign_type']        = $this->sign_type;
        $this->params['body']             = $this->body;
        $this->params['detail']           = $params['detail'];
//        $this->params['attach']           = $params['attach'];
        $this->params['out_trade_no']     = $this->out_trade_no;
        $this->params['fee_type']         = $this->fee_type;
        $this->params['total_fee']        = $this->total_fee;
        $this->params['spbill_create_ip'] = $this->spbill_create_ip;
        $this->params['time_start']       = $this->time_start;
        $this->params['time_expire']      = $this->time_expire;
//        $this->params['goods_tag']        = $this->goods_tag;
        $this->params['notify_url']       = $this->notify_url;
        $this->params['trade_type']       = $this->trade_type;
        //获取签名数据
        $this->sign                       = $this->getSign($this->params);
        $this->params['sign']             = $this->sign;
        $xml = $this->getXml($this->params);
        $response = $this->postXmlCurl($xml, self::API_URL_PREFIX.self::UNIFIEDORDER_URL);
        if(!$response){
            return ['code' => 1,'msg'=>'网络错误'];
        }
        $result = $this->getArr( $response );
        if(!empty($result['result_code']) && !empty($result['err_code']) ){
            return ['code' => 1,'msg'=>$this->error_code( $result['err_code'] )];
        }elseif($result['return_code'] == "FAIL"){
            return ['code' => 1,'msg'=>$result['return_msg']];
        }
        //生成图片
        $QRcode = new \QRcode();
        ob_start(); // 在服务器打开一个缓冲区来保存所有的输出
        $QRcode->png($result['code_url'],false,"l",13);
        $imageString = base64_encode(ob_get_contents());
        ob_end_clean(); //清除缓冲区的内容，并将缓冲区关闭，但不会输出内容
        return [
            'code' => 0,
            'msg'=>'',
            'data'=>"data:image/jpg;base64,".$imageString
        ];
    }

    # 生成签名  @return 签名
    public function getSign( $params ){
        //签名步骤一：按字典序排序数组参数
        ksort($params);
        $array = array();
        foreach( $params as $key => $value ){
            $array[] = $key.'='.$value;
        }
        $string = implode("&",$array);
        //签名步骤二：在string后加入KEY
        $string = $string . "&key=".$this->key;
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    # 错误类型   服务器输出的错误代码
    public function error_code( $code ){
        $errList = array(
            'INVALID_REQUEST'       =>  '参数错误',
            'NOAUTH'                =>  '商户未开通此接口权限',
            'NOTENOUGH'             =>  '用户帐号余额不足',
            'ORDERPAID'             =>  '商户订单已支付，无需重复操作',
            'ORDERCLOSED'           =>  '当前订单已关闭，无法支付',
            'SYSTEMERROR'           =>  '系统错误!系统超时',
            'APPID_NOT_EXIST'       =>  '参数中缺少APPID',
            'MCHID_NOT_EXIST'       =>  '参数中缺少MCHID',
            'APPID_MCHID_NOT_MATCH' =>  'appid和mch_id不匹配',
            'LACK_PARAMS'           =>  '缺少必要的请求参数',
            'OUT_TRADE_NO_USED'     =>  '订单号重复操作',
            'SIGNERROR'             =>  '参数签名错误',
            'XML_FORMAT_ERROR'      =>  'XML格式错误',
            'REQUIRE_POST_METHOD'   =>  '未使用post传递参数 ',
            'POST_DATA_EMPTY'       =>  'post数据不能为空',
            'NOT_UTF8'              =>  '未使用指定编码格式',
        );
        if( array_key_exists( $code , $errList ) ){
            return $errList[$code];
        }
    }

    # 订单状态  获取订单状态
    public function order_status($params = []){
        $this->params['appid']            = $this->appid;
        $this->params['mch_id']           = $this->mch_id;
        $this->params['out_trade_no']           = $params['order_no'];
        $this->nonce_str             = $this->getRandStr();
        $this->params['nonce_str']        = $this->nonce_str;
        $this->sign                       = $this->getSign($this->params);
        $this->params['sign']             = $this->sign;
        $xml = $this->getXml($this->params);
        $response = $this->postXmlCurl($xml, self::API_URL_PREFIX.self::ORDER_QUERY);
        if(!$response){
            return false;
        }
        $result = $this->getArr( $response );
        return $result;
    }

    # 以post方式提交xml到对应的接口url
    # @param string $xml  需要post的xml数据
    # @param string $url  url
    # @param bool $useCert 是否需要证书，默认不需要
    # @param int $second   url执行超时时间，默认30s
    private function postXmlCurl($xml, $url, $useCert = false, $second = 30){
        $ch = curl_init();
        # 设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        # 设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        # 要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if($useCert == true){
            # 设置证书
            # 使用证书：cert 与 key 分别属于两个.pem文件
            curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM'); # value 证书地址
            # curl_setopt($ch,CURLOPT_SSLCERT, WxPayConfig::SSLCERT_PATH);
            curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');  # value 证书地址
            # curl_setopt($ch,CURLOPT_SSLKEY, WxPayConfig::SSLKEY_PATH);
        }
        # post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        # 运行curl
        $data = curl_exec($ch);
        # 返回结果
        if($data){
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            return false;
        }
    }

    # 输出xml字符  return   string      返回组装的xml
    public function getXml( $params ){
        if(!is_array($params)|| count($params) <= 0)return false;
        $xml = "<xml>";
        foreach ($params as $key=>$val)
        {
            if (is_numeric($val)){
                $xml.="<".$key.">".$val."</".$key.">";
            }else{
                $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
            }
        }
        $xml.="</xml>";
        return $xml;
    }

    # 将xml转为array return array
    public function getArr($xml){
        if(!$xml)return false;
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $data;
    }

    # 执行第二次签名，才能返回给客户端使用
    public function getOrder($prepayId){
        $data["appid"]     = $this->appid;
        $data["noncestr"]  = $this->getRandStr(32);
        $data["package"]   = "Sign=WXPay";
        $data["partnerid"] = $this->mch_id;
        $data["prepayid"]  = $prepayId;
        $data["timestamp"] = time();
        $data["sign"] = $this->getSign($data);
        return $data;
    }

    # 获取指定长度的随机字符串
    function getRandStr($length=32){
        $str = null;
        $strPol = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $max = strlen($strPol)-1;
        for($i=0;$i<$length;$i++){
            $str.=$strPol[rand(0,$max)];//rand($min,$max)生成介于min和max两个数之间的一个随机整数
        }
        return $str;
    }
}
