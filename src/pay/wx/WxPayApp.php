<?php
namespace doujunyu\utility\pay\wx;
/**
 * php开发微信app支付接口
 * @global  WX_APPID  开放平台->移动应用appid
 * @global  WX_MCHID  微信支付商户号
 * @global  WX_KEY    商户支付秘钥
 * @author codehi <admin@codehui.net> 2017-12-23
 */
class WxPayApp
{
    /**
     * 微信支付统一下单 >>> 生成预支付交易单
     */
    public function wx_pay(){

        $request_data = array(
            'appid' => 'wx861ca73a2c404953',                         #应用APPID
            'mch_id' => '1595370531',                        #商户号
            'trade_type' => 'APP',                            #支付类型
            'nonce_str' => md5('ssdfsefsdse'.rand(1,9999)),  #随机字符串 不长于32位
            'body' => '商品名称',                             #商品名称
            'out_trade_no' => 'demo-dou'.time().rand(1,9999),                    #商户后台订单号
            'total_fee' => '1',                             #商品价格
            'spbill_create_ip' => get_client_ip(),            #用户端实际ip
            'notify_url' => 'http://admin.yunbao.live/index.php/pay/wx_pay_back/back', #异步通知回调地址
        );
        // 获取签名
        $request_data['sign'] = $this -> get_sign($request_data);
        // 拼装数据
        $xml_data = $this -> set_xmldata($request_data);


        // 发送请求
        $res = $this -> send_prePaycurl($xml_data);

        if($res['return_code'] == 'SUCCESS' && $res['result_code'] == 'SUCCESS'){
            $two_data['appid'] = 'wx861ca73a2c404953';  #APPID
            $two_data['partnerid'] = '1595370531';  #商户号
            $two_data['prepayid'] = $res['prepay_id'];  //预支付交易会话标识
            $two_data['noncestr'] = $res['nonce_str'];
            $two_data['timestamp'] = time();
            $two_data['package'] = "Sign=WXPay";
            $two_data['sign'] = $this->get_twosign($two_data);
return $two_data;
//            $this->ajaxReturn(array('code'=>200,'info'=>$two_data));
        }else{
return $res;
//            $this->ajaxReturn(array('code'=>201,'info'=>$res['err_code_des']));
        }
    }

    //通过curl发送数据给微信接口的函数
    private function send_prePaycurl($xmlData) {
        $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        $header[] = "Content-type: text/xml";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlData);
        $data = curl_exec($curl);
        if (curl_errno($curl)) {
            print curl_error($curl);
        }
        curl_close($curl);
        return $this->_xmldataparse($data);
    }

    //xml数据解析函数
    private function _xmldataparse($data){
        $msg = array();
        $msg = (array)simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
        return $msg;
    }

    //生成xml格式的函数
    private function set_xmldata($data) {
        $xmlData = "<xml>";
        foreach ($data as $key => $value) {
            $xmlData.="<".$key."><![CDATA[".$value."]]></".$key.">";
        }
        $xmlData = $xmlData."</xml>";
        return $xmlData;
    }

    //一次签名的函数
    private function get_sign($data){
        ksort($data);
        $str = '';
        foreach ($data as $key => $value) {
            $str .= !$str ? $key . '=' . $value : '&' . $key . '=' . $value;
        }
        $str.='&key=henankaishankejiyunbaolive123456';
        $sign = strtoupper(md5($str));
        return $sign;
    }

    //二次签名的函数
    private function get_twosign($data){
        $sign_data = array(
            "appid"=>$data['appid'],
            "partnerid"=>$data['partnerid'],
            "prepayid"=>$data['prepayid'],
            "noncestr"=>$data['noncestr'],
            "timestamp"=>$data['timestamp'],
            "package"=>$data['package'],
        );
        return $this->get_sign($sign_data);
    }

    //微信回调
    public function wx_notify(){
        //允许从外部加载XML实体(防止XML注入攻击)
        libxml_disable_entity_loader(true);
        $postStr = $this -> post_data();//接收post数据
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        $arr = $this -> object_toarray($postObj);//对象转成数组
        ksort($arr);// 对数据进行排序
        $str = $this -> params_tourl($arr);//对数据拼接成字符串
        $user_sign = strtoupper(md5($str));
        if($user_sign == $arr['sign']){//验证签名
            return '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
        }else{
            return '<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
        }
    }

    // 接收post数据
    /*
    *  微信是用$GLOBALS['HTTP_RAW_POST_DATA'];这个函数接收post数据的
    */
    public function post_data(){
        $receipt = $_REQUEST;
        if($receipt==null){
            $receipt = file_get_contents("php://input");
            if($receipt == null){
                $receipt = $GLOBALS['HTTP_RAW_POST_DATA'];
            }
        }
        return $receipt;
    }

    //把对象转成数组
    public function object_toarray($arr) {
        if(is_object($arr)) {
            $arr = (array)$arr;
        } if(is_array($arr)) {
            foreach($arr as $key=>$value) {
                $arr[$key] = $this->object_toarray($value);
            }
        }
        return $arr;
    }


    /**
     * 格式化参数格式化成url参数
     */
    private function params_tourl($arr)
    {
        $weipay_key = C('WX_KEY');//微信的key,这个是微信支付给你的key，不要瞎填。
        $buff = "";
        foreach ($arr as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }
        $buff = trim($buff, "&");
        return $buff.'&key='.$weipay_key;
    }

}