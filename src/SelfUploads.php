<?php
/**
 * 图片相关服务
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019-04-30
 * Time: 16:39
 */
namespace doujunyu\utility;

class SelfUploads{

    /**
     * 图片上传
     * @return string
     */
    public static function upload_tp5($file = [],$group = 'common')
    {
        if(empty($file)){
            return ['code'=>1,'msg'=>'没有找到图片或文件'];
        }
        $data=[];
        $default_suffix = pathinfo($file->getInfo()['name'], PATHINFO_EXTENSION);//文件后缀
        if(isset($check['suffix']) && ($default_suffix !== $check['suffix'])){
            return ['code'=>1,'msg'=>'图片格式错误'];
        }
        // 获取表单上传文件 例如上传了001.jpg
//        $file = request()->file('file');
        // 移动到框架应用根目录/public/uploads 目录下
        // 斜杠  DIRECTORY_SEPARATOR
        $url = self::path($group.'/'.date('Ymd',time()));//路径
        $info = $file->move(dirname($_SERVER['SCRIPT_FILENAME']) . $url,self::name($default_suffix));
        if ($info) {
            $path = $info->getSaveName();
            // 成功上传后 返回上传信息
            $pathImage = $url . $path;
            return ['code'=>0,'msg'=>'上传成功','url'=>$pathImage];

        } else {
            // 上传失败返回错误信息
            $data['code'] = 1;
            $data['msg'] = $file->getError();

            return ['code'=>2,'msg'=>$file->getError()];
        }
        return ['code'=>3,'msg'=>'参数错误','url'=>''];
    }

    /**
     * 图片base64上传
     */
    public static function upload_base64($file = '',$group = 'common'): array
    {
//        $file = $this->request->post('logo_base64','');
        //$logo_data = $_POST['logo_base64'];

        if(!empty($file)){
            //$data = file_get_contents('./1.txt');
//            $reg = '/data:image\/(\w+?);base64,(.+)$/si';//如果存在data:image开头进行分离
//            preg_match($reg,$file,$match_result);

            $url = self::path($group.DIRECTORY_SEPARATOR.date('Ymd',time()));//路径
            $logo_path = $url.self::name('jpeg');//路径+名字
            $num = file_put_contents('.'.$logo_path,base64_decode($file));
            $data = [];
            if(!empty($num)){
                //上传成功之后，再进行缩放操作
                //$image = \think\Image::open($logo_path);

                // 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.png
                //$image->thumb(102, 36)->save($logo_path);
                $data['code'] = 0;
                $data['msg']='上传成功';
                $data['data'] = $logo_path;
                return ['code'=>0,'msg'=>'上传成功','url'=>$logo_path];
            }else{
                return ['code'=>3,'msg'=>'图片上传失败','url'=>''];
            }
        }
        return ['code'=>2,'msg'=>'参数错误','url'=>''];

    }

    /**
     * tp6.0  图片上传
     * @param $picture
     * @param string $group 分组
     * @param string $suffix 文件后缀
     * @param string $fileExt 文件类型
     * @param string $fileSize 文件大小
     * @return array
     */
    public static function upload_tp6($picture, string $group = 'common', string $suffix = 'jpeg', string $fileExt = 'jpg,png,gif,jpeg', string $fileSize = '20971520'): array
    {
        $url = '';
        $path = date('Ymd',time()).'/'.self::name($suffix);
        try {
            validate(['file' => ["fileSize:$fileSize", "fileExt:$fileExt"]])->check(['file' => $picture]);
            $url = \think\facade\Filesystem::disk('public')->putFileAs( $group, $picture,$path);
        } catch (\think\exception\ValidateException $e) {
            return ['code' => 1,'msg' => $e->getMessage()];
        }
        return ['code' => 0,'url' => $url];
    }
    /**
     * 文件转移
     * @param string $old_url
     * @param string $group
     * @return array
     */
    public static function copy_stick(string $old_url, string $new_group, string $old_group = "common"): array
    {
        $old_url = trim($old_url,'/');
        if(empty($old_url) || !file_exists('./'.$old_url)){
            return ['code'=>1,'msg'=>'图片未找到'];
        }
        if(empty($new_group)){
            return ['code'=>2,'msg'=>'移动文件位置不能为空'];
        }

        $fileName = basename($old_url);
        $new_url = str_replace($old_group,$new_group,$old_url);
        $url = self::path(dirname($new_url));//路径
        $group_url = $url.$fileName;
        if($old_url != $group_url){
            if(!copy('./'.$old_url,'./'.$group_url)){
                return ['code'=>3,'msg'=>'转移失败'];
            }
        }
        return ['code'=>0,'msg'=>'转移成功','url'=>$group_url];
    }
    /**
     * 删除文件
     */
    public static function del($file): array
    {
        $file_strpos = strpos($file,'upload');
        if(empty($file_strpos)){
            return ['code'=>1,'msg'=>'文件路径没有找到upload'];
        }
        $file_substr = substr($file,$file_strpos);
        if(!file_exists('./'.$file_substr)){
            return ['code'=>2,'msg'=>'文件不存在'];
        }
        if(!unlink('.'.$file)){
            return ['code'=>3,'msg'=>'删除失败'];
        }
        return ['code'=>0,'msg'=>'删除成功'];

    }
    /**
     * 富文本接口返回
     */
    public function imageUrl($content){
        $reg = strpos($content,'http');
        if(empty($reg)){
            $url = "http://" . $_SERVER['SERVER_NAME'];
        }else{
            $url = '';
        }
        $pregRule = "/<[img|IMG].*?src=[\'|\"](.*?(?:[\.jpg|\.jpeg|\.png|\.gif|\.bmp]))[\'|\"].*?[\/]?>/";
        $content = preg_replace($pregRule, '<img src="' . $url . '${1}" style="max-width:100%">', $content);
        return $content;
    }
    /**
     * 抠图
     */
    public static function imageMatting($path,$group){

        $image = file_get_contents($path);

        $info = getimagesize($path);

        $im = imagecreatefromstring($image);

        $width = $info[0];

        $height = $info[1];

        for($i=0;$i<$height;$i+=1){

            for($j=0;$j<$width;$j+=1){

                $rgb = ImageColorAt($im, $j, $i);

                $r = ($rgb >> 16) & 0xFF;

                $g = ($rgb >> 8) & 0xFF;

                $b = $rgb & 0xFF;

//                echo $r.'.'.$g.'.'.$b.'.='.$rgb.'
//x='.$j.', y='.$i.'
//';



                if(intval($r)>220 && $g >220 && $b>220){

                    $hex = imagecolorallocate($im, 255, 255, 255);

                    imagesetpixel($im,$j, $i, $hex);

                }

            }

        }

        $white = imagecolorallocate($im , 255 , 255 , 255);//拾取白色

        imagefill($im , 0 , 0 , $white);//把画布染成白色

        imagecolortransparent($im , $white ) ;//把图片中白色设置为透明色
        $url = self::path($group.DIRECTORY_SEPARATOR.date('Ymd',time()));//路径

        $group_url = $url.rand(111,222).'.png';
        imagepng($im , '.'.$group_url);//生成图片
        return ['code'=>0,'msg'=>'操作成功','url'=>$group_url];

    }

    /**
     * 网络图片保存到服务器
     * @throws \Exception
     */
    public static function httpImage($img,$group): string
    {
        $date = date('Ymd',time());
        $rand = time().random_int(1000,9999);
        $image = file_get_contents($img);
        $url = self::path($group.DIRECTORY_SEPARATOR.$date);
        $img_name = $rand.'.png';
        file_put_contents('.'.$url.$img_name, $image);
        return $url.$img_name;
    }
    /**
     * 创建图片目录
     */
    public static function path($path): string
    {
        // 移动到框架应用根目录/public/uploads 目录下
//        $dir = DIRECTORY_SEPARATOR .$path.DIRECTORY_SEPARATOR ;
        // 移动到框架应用根目录/public/uploads 目录下
        $dir = "/$path/" ;
//        if (!is_dir('.'.$dir )) {  #创建目录
//            mkdir('.'.$dir,0777,true);
//        }
        if (!is_dir('.' . $dir) && !mkdir($concurrentDirectory = '.' . $dir, 0777, true) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        return $dir;
    }

    /**
     * 判断两个图片的相似度  完全一样返回100 差距越大返回的数越低
     * @param $imagePath1
     * @param $imagePath2
     * @return float|int
     */
    public static function compareImages($imagePath1, $imagePath2) {
        $image1 = imagecreatefromjpeg($imagePath1);
        $image2 = imagecreatefromjpeg($imagePath2);

        $difference = 0;
        $width = imagesx($image1);
        $height = imagesy($image1);

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $rgb1 = imagecolorat($image1, $x, $y);
                $rgb2 = imagecolorat($image2, $x, $y);

                $r1 = ($rgb1 >> 16) & 0xFF;
                $g1 = ($rgb1 >> 8) & 0xFF;
                $b1 = $rgb1 & 0xFF;

                $r2 = ($rgb2 >> 16) & 0xFF;
                $g2 = ($rgb2 >> 8) & 0xFF;
                $b2 = $rgb2 & 0xFF;

                $difference += abs($r1 - $r2) + abs($g1 - $g2) + abs($b1 - $b2);
            }
        }
        $totalPixels = $width * $height;
        $similarity = ($totalPixels - ($difference / 3)) / $totalPixels * 100;
        return $similarity;
    }


    /**
     * 随机生成图片名
     * @throws \Exception
     */
    private static function name($suffix){
        return date('H-i-s',time()).'-'.random_int(100000,999999).'.'.$suffix;//名字
    }



}