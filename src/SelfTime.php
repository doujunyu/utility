<?php
/**
 * 图片相关服务
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019-04-30
 * Time: 16:39
 */
namespace doujunyu\utility;

class SelfTime{

    //今天时间
    public static function today(){
        $time['start'] = strtotime(date('Y-m-d 00:00:00',time()));
        $time['over'] = strtotime(date('Y-m-d 23:59:59',time()));
        return $time;
    }
    //昨天时间
    public static function yesterday(){
        $time['start'] = strtotime(date('Y-m-d 00:00:00',time()-3600*24));
        $time['over'] = strtotime(date('Y-m-d 23:59:59',time()-3600*24));
        return $time;
    }

    //本周
    public static function week(){
        $timestamp = time();
        $time['start'] = strtotime(date('Y-m-d', strtotime("this week Monday", $timestamp)));
        $time['over'] = strtotime(date('Y-m-d', strtotime("this week Sunday", $timestamp))) + 24 * 3600 - 1;
        return $time;
    }
    //本月
    public static function month(){
        $time['start'] = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $time['over'] = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
        return $time;
    }
    //某月的开头和结尾
    public static function whoMonth($whoTime){
        $time['start'] = mktime(0, 0, 0, date('m',$whoTime), 1, date('Y',$whoTime));
        $time['over'] = mktime(23, 59, 59, date('m',$whoTime), date('t',$whoTime), date('Y'));
        return $time;
    }
    //上月
    public static function lastMonth(){
        $time['start'] = strtotime(date('Y-m-01') . '-1 month');
        $time['over'] = strtotime(date('Y-m-01'))-1;
//        $time['over'] = mktime(23, 59, 59, date('m'), 0, date('Y'));
        return $time;
    }

    //今天早上0点到6点
    public static function todayMorning(){
        $time['start'] = strtotime(date('Y-m-d 00:00:00',time()));
        $time['over'] = strtotime(date('Y-m-d 6:00:00',time()));
        return $time;
    }

    //今天上午6点到12点
    public static function todayForenoon(){
        $time['start'] = strtotime(date('Y-m-d 6:00:00',time()));
        $time['over'] = strtotime(date('Y-m-d 12:00:00',time()));
        return $time;
    }

    //今天下午12点到18点
    public static function todayAfternoon(){
        $time['start'] = strtotime(date('Y-m-d 12:00:00',time()));
        $time['over'] = strtotime(date('Y-m-d 18:00:00',time()));
        return $time;
    }

    //今天晚上18点到24点
    public static function todayEvening(){
        $time['start'] = strtotime(date('Y-m-d 18:00:00',time()));
        $time['over'] = strtotime(date('Y-m-d 24:00:00',time()));
        return $time;
    }

    /**
     * @return 昨天
     */
    //昨天早上0点到6点
    public static function yesterdayMorning(){
        $time['start'] = strtotime(date('Y-m-d 00:00:00',time()))-3600*24;
        $time['over'] = strtotime(date('Y-m-d 6:00:00',time()))-3600*24;
        return $time;
    }

    //昨天上午6点到12点
    public static function yesterdayForenoon(){
        $time['start'] = strtotime(date('Y-m-d 6:00:00',time()))-3600*24;
        $time['over'] = strtotime(date('Y-m-d 12:00:00',time()))-3600*24;
        return $time;
    }

    //昨天下午12点到18点
    public static function yesterdayAfternoon(){
        $time['start'] = strtotime(date('Y-m-d 12:00:00',time()))-3600*24;
        $time['over'] = strtotime(date('Y-m-d 18:00:00',time()))-3600*24;
        return $time;
    }

    //昨天晚上18点到24点
    public static function yesterdayEvening(){
        $time['start'] = strtotime(date('Y-m-d 18:00:00',time()))-3600*24;
        $time['over'] = strtotime(date('Y-m-d 24:00:00',time()))-3600*24;
        return $time;
    }
    /**
     *      把秒数转换为时分秒的格式
     *      @param Int $times 时间，单位 秒
     *      @return String
     */
    public static function secToTime($times){
        $result = '00:00:00';
        if ($times>0) {
            $hour = floor($times/3600);
            $minute = floor(($times-3600 * $hour)/60);
            $second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
            $result = $hour.':'.$minute.':'.$second;
        }
        return $result;
    }

    /**
     * 是否为闰年
     * @static
     * @access public
     * @return string
     */
    public static function isLeapYear($year = '')
    {
        return (((($year % 4) == 0) && (($year % 100) != 0)) || (($year % 400) == 0));
    }

    /**
     * 日期数字转中文
     * 用于日和月、周
     * @static
     * @access public
     * @param integer $number 日期数字
     * @return string
     */
    public static function numberToCh(int $number): string
    {
        $number = (int)$number;
        $array = array('一', '二', '三', '四', '五', '六', '七', '八', '九', '十');
        $str = '';
        if ($number == 0) {
            $str .= "十";
        }
        if ($number < 10) {
            $str .= $array[$number - 1];
        } elseif ($number < 20) {
            $str .= "十" . $array[$number - 11];
        } elseif ($number < 30) {
            $str .= "二十" . $array[$number - 21];
        } else {
            $str .= "三十" . $array[$number - 31];
        }
        return $str;
    }

    /**
     * 年份数字转中文
     * @static
     * @access public
     * @param integer $yearStr 年份数字
     * @param boolean $flag 是否显示公元
     * @return string
     */
    public static function yearToCh($yearStr, $flag = false)
    {
        $array = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
        $str = $flag ? '公元' : '';
        for ($i = 0; $i < 4; $i++) {
            $str .= $array[substr($yearStr, $i, 1)];
        }
        return $str;
    }

    /**
     *  判断日期 所属 干支 生肖 星座
     *  type 参数：XZ 星座 GZ 干支 SX 生肖
     *
     * @static
     * @access public
     * @param string $type 获取信息类型
     * @return string
     */
    public static function magicInfo($type,$year,$month,$day)
    {
        $result = '';
        $m = $month;
        $y = $year;
        $d = $day;

        switch ($type) {
            case 'XZ'://星座
                $XZDict = array('摩羯', '宝瓶', '双鱼', '白羊', '金牛', '双子', '巨蟹', '狮子', '处女', '天秤', '天蝎', '射手');
                $Zone = array(1222, 122, 222, 321, 421, 522, 622, 722, 822, 922, 1022, 1122, 1222);
                if ((100 * $m + $d) >= $Zone[0] || (100 * $m + $d) < $Zone[1])
                    $i = 0;
                else
                    for ($i = 1; $i < 12; $i++) {
                        if ((100 * $m + $d) >= $Zone[$i] && (100 * $m + $d) < $Zone[$i + 1])
                            break;
                    }
                $result = $XZDict[$i] . '座';
                break;

            case 'GZ'://干支
                $GZDict = array(
                    array('甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸'),
                    array('子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥')
                );
                $i = $y - 1900 + 36;
                $result = $GZDict[0][$i % 10] . $GZDict[1][$i % 12];
                break;

            case 'SX'://生肖
                $SXDict = array('鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊', '猴', '鸡', '狗', '猪');
                $result = $SXDict[($y - 4) % 12];
                break;

        }
        return $result;
    }

    /**
     * 日期加减操作
     * @param string $date 要处理的日期
     * @param int $day >  加天  <0 减天
     * @param string $format 要返回的格式  Y-m-d
     * @return string
     */
    public static function dateAdd($date, $day = 0, $format = 'Y-m-d')
    {
        if (!empty($date) && $day != 0) {
            $str = $day > 0 ? '+' . $day : $day;
            $time = strtotime($str . ' day', strtotime($date));
        } elseif (empty($date)) {
            $time = strtotime($date);
        } else {
            $time = time();
        }
        return date($format, $time);

    }


}