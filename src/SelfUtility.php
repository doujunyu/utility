<?php

namespace doujunyu\utility;



class SelfUtility
{

    /**
     * 随机字符串
     * @param int $len //长度
     * @param null $chars //随机的值
     * @return string
     */
    public static function getRandomString($len, $chars=null)
    {
        if (is_null($chars)) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }
        mt_srand(10000000*(double)microtime());
        for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, $lc)];
        }
        return $str;
    }

    /**
     * 处理流量格式
     * $size 单位是kb
     */
    public static function bytes($size) {
        $units = array(' KB', ' MB', ' GB', ' TB');
        for ($i = 0; $size >= 1024 && $i < 3; $i++) $size /= 1024;
        return round($size, 2).$units[$i];
    }

    /**
     * 处理流量转换
     * $size 单位是k
     */
    public static function bitGetGb($size){
        return round($size/(1024 * 1024 * 1024), 2);
    }

    /**
     * php 输出视频流
     * @param $file
     */
    public static function video($file){
        ini_set('memory_limit','512M');
        header("Content-type: video/mp4");
        header("Accept-Ranges: bytes");

        ob_start();// ------ 开启缓冲区
        $size = filesize($file);

        if(isset($_SERVER['HTTP_RANGE'])){
            header("HTTP/1.1 206 Partial Content");
            list($name, $range) = explode("=", $_SERVER['HTTP_RANGE']);
            list($begin, $end) =explode("-", $range);
            if($end == 0) $end = $size - 1;
        }
        else {
            $begin = 0; $end = $size - 1;
        }

        header("Content-Length: " . ($end - $begin + 1));
        header("Content-Disposition: filename=".basename($file));
        header("Content-Range: bytes ".$begin."-".$end."/".$size);

        try {
            $fp = fopen($file, 'r');
        } catch (\Exception $e) {
            echo $e->getTraceAsString();exit;
        }
        fseek($fp, $begin);
        $contents = '';

        while(!feof($fp)) {
            $p = min(1024, $end - $begin + 1);
            //$begin += $p;
            $contents .= fread($fp, $p);
            //echo fread($fp, $p);
        }
        //$contents = ltrim($contents, "\XEF\XBB\XBF");
        ob_end_clean();            // ------ 清除缓冲区
        ob_clean();
        //$contents = substr($contents, 3);
        fclose($fp);

        exit($contents);
    }

    /**
     *求两个已知经纬度之间的距离,单位为米
     * @param $longitude1 经度
     * @param $latitude1 纬度
     * @param $longitude2 经度
     * @param $latitude2 纬度

     * @return float 距离，单位米
     **/
    public static function coordinateDistance($longitude1, $latitude1, $longitude2, $latitude2) {
        $lng1 = $longitude1;//经度
        $lat1 = $latitude1;//维度
        $lng2 = $longitude2;//经度
        $lat2 = $latitude2;//维度
        //将角度转为狐度

        $radLat1=deg2rad($lat1);//deg2rad()函数将角度转换为弧度

        $radLat2=deg2rad($lat2);

        $radLng1=deg2rad($lng1);

        $radLng2=deg2rad($lng2);

        $a=$radLat1-$radLat2;

        $b=$radLng1-$radLng2;

        $s=2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137*1000;

        return round($s, 2);

    }

}