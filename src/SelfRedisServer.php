<?php
namespace doujunyu\utility;

class SelfRedisServer{
    public function Lock($key,$time = 10){
        while (true){
            $this_time = time();
            if(!SelfRedis::setnx($key,($this_time + $time))){
                $s_time = SelfRedis::ttl($key);
                if($s_time > 0) {
                    sleep($s_time);
                }else{
                    sleep(1);
                }
            }else{
                SelfRedis::expire($key,$time);
                break;
            }
        }
    }

}